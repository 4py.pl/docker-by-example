# Docker - by example

wersja: robocza

autor: Michał Nadolski

#### Pytania

- Czym jest docker?
    - Docker to platforma do konteneryzacji aplikacji (alternatywa dla wirtualizacji).
    - kontener to taki sandbox, zasboy które są izolowane: logika, sieć (dns, ip, port), cpu, ram, hdd)
    - VM vs container
        - performance (start w minutach vs w milisekundach)
        - izolacja (emulacja pełnego hardware'u vs tylko systemu, który jest zasobem współdzielonym i readonly)
        - alokacja pamięci (duża vs mała)
        - bezpieczeństwo (duże, niższe?)
    - narzędzia współtowarzyszące:
        - docker desktop (bundle: cli, engine, compose, kubernetes, swarm, wsl 2, hyper-v)
        - docker cli (shell)
        - docker engine (api)
        - docker machine (hardware)
        - docker driver (virtualbox, aws, gcp, nie mylić z docker storage driver)
        - tocker toolbox
        - docker compose
        - docker hub
        - docker swarm
- Kiedy powstał?
    - docker - 2013 marzec
    - lxc - 2008 sierpień
- Kto jest właścicielem?
    - aktualnie (od 2019) firma Mirantis, zainteresowania: chmura, głównie: (OpenStack, kubernetes)
    - założycielem jest Solomon Hykes / DotCloud, zainteresowania: PaaS dla developerów (coś a'la heroku?)
- Czy jest darmowy?
    - docker to projekt open-source
    - bezpłatna - Community Edition, płatna: Enterprise Edition
    - różnica CE vs EE: głównie chodzi o certyfikaty, support i narzędzia wspomagające
- Czy jest popularny?
    - tak
    - google trends
    - stackoverflow survey
    - popularność wysoka, szczyt w 2019 (aktualnie w 2020 minimalnie spadła)
- Kto używa?
    - procent rynku
    - segment rynku
    - duże firmy, przykłady
    - powiązane technologie (języki programowania - jest na to infografika, inne?)
- Czy warto się uczyć dockera?
    - tak, zawsze
    - na dzień dzisiejszy prawie must-have w IT
- Kto powinien znać dockera?
    - przegląd stanowisk pracy na przykładzie nofluffjobs
    - architekt (must have)
    - admin (must have)
    - devops (must have)
    - dev full stack (must have)
    - dev backend (must have)
    - dev frontend
    - tester 
    - db engineer
    - analityk
    - pm
- Czy łatwo jest się nauczyć dockera?
    - i tak i nie
    - tak, bo: niski próg wejścia, świetna dokumentacja, dojrzały produkt - rzadko występują błędy, świetne community
    - nie, bo: sporo możliwości - a więc sporo do przyswojenia, szerokie pole zastosowania, zaawansowane przypadki użycia
- Jaka jest alternatywa dla dockera?
    - bare metal
    - VM
    - CoreOS
    - Mesos
    - kubernetes
    - lxc
    - public cloud SaaS
    - serverless (???)
    - high level soulution (rancher, aws, gcp, azure)
- Co daje docker?
    - generalnie: czas, a więc oszczędność zasobów
- Czy umiejętność używania zwiększy moją szansę na rynku pracy?
    - bezwątpienia tak

#### Podsumowanie benefitów dockera

- Wydajność
    - czas uruchomienia VMki to minuty vs docker to sekundy
- Skalowalność
    - dzisiaj kilkadziesiąt odsłon, jutro reklama w telewizji śniadaniowej i milion odsłon na godzinę
- Izolacja procesów
    - łatwa podmiana nie działąjącego kontenera
    - ograniczone przywileje, czyli większe bezpieczeństwo
- Wysoka dostępność
    - infrastruktura bardziej odporna na awarię
    - krótki czas naprawy (recovery time) - czy to tu na pewno ma się pojawić?
- Szybki deployment
    - Dockerfile
    - yaml
    - automatyzacja
- Przenośność
    - działa u mnie na laptopie z linuxem
    - działa w dziale QA na widnowsach
    - działa u szefa na macu
    - działa na serwerach w firmie
    - może działać w chmurze (u wszystkich providerów: AWS, GCP, AZURE, OVH, itp.)
- Automatyzacja
    - tworzenie środowisk na życzenie (w locie)
    - integracja z zewnętrznymi narzędziami
    - testowanie
    - przyśpieszanie najczęście powtarzających się czynności
    - skracanie najdłużej wykonujących się zadań
- Limitowanie zasobów
    - RAM, CPU, GPU
- Waratość marketingowa
    - zwiększa wartość firmy
    - prestiż
- Dług technologiczny (to potrzebne?)
- Elastyczność
    - Daje możliwości, ale nie zobowiązuje do ich stosowania
    - Współdzielenie woluminów
    - Zbieranie metryk
- Świetna dokumentacja
    - chciałbym, aby każdy projekt informatyczny był tak dobrze opisany jak docker

#### Przykład

- Hello world
    - odpalenie gotowego rozwiązania w sekundach
    - prezentacja Dockerfile
    - omówienie
    - porównanie vs skrypt dla bare metal

- Full CRUD example
    - przykład architektury mikroserwisowej, nie skomplikowany CRUD, logowanie, SSL
    - proxy: haproxy lub nginx 
    - frontend w angularze
    - backend w pythonie (?)
    - baza danych (PostgreSQL)
    - serwis do generowania PDF
    - jakiś serwis w go

#### Co dalej (propozycje)

- Docker w praktyce - warsztaty
- Docker Swarm vs Kubernetes
- Automatyzacja: Chef vs Ansible vs Terraform, CI/CD
- GitLab (all in one)
- Wyższy poziom abstrakcji: Rancher
- Izolowane kontenery (prace nad następcą dockera/inną technologią, firmy takie jak: google, ibm, oracle, openstack i inne opracowują nowy projekt)

#### Grafiki:

- [docker desktop: mac vs windows](https://i0.wp.com/www.docker.com/blog/wp-content/uploads/2020/01/Docker-Stat-Graphic-012920-V1_Blog-Users-Want-Choice-scaled.png?resize=1110%2C740&ssl=1)
- [docker 2020 w liczbach](https://i2.wp.com/www.docker.com/blog/wp-content/uploads/2020/01/Docker-Stat-Graphic-012920-V1_Blog-Mainstream-and-Growing-scaled.png?resize=1110%2C740&ssl=1)
- [co jest uruchamiane najczęściej](https://miro.medium.com/max/2400/1*_Kk4NIkBdoJzKS1oNQ57gQ.jpeg)
- [co zarządza dockerem](https://miro.medium.com/max/700/1*BG6olFNxyra9LS-NFTQciw.png)
